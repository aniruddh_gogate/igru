﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using App2.Data;
using System.IO;
using Xamarin.Forms;
using App2.iOS.Data;

[assembly: Dependency(typeof(SQLIte_IOS))]

namespace App2.iOS.Data
{
    public class SQLIte_IOS : ISQLite
    {
        public SQLIte_IOS() { }
        public SQLite.SQLiteConnection GetConnection()
        {
            var fileName = "grassroots.db";
            var documentPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentPath, "..", "Library");
            var path = Path.Combine(libraryPath, fileName);

            var connection = new SQLite.SQLiteConnection(path);

            return connection;

        } 
    }
}