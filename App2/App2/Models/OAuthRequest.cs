﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Model
{
    public class OAuthRequest
    {
        public OAuthRequest() { }

        public OAuthRequest(string username, string password)
        {
            Grant_type = "password";
            Username = username;
            Password = password;
            Client_id = "MobileApp";
            Client_secret = "FhQX3R7ELRhCTXMq";
        }

        [AliasAs("Grant_type")]
        public string Grant_type { get; set; }

        [AliasAs("Username")]
        public string Username { get; set; }

        [AliasAs("Password")]
        public string Password { get; set; }

        [AliasAs("Client_id")]
        public string Client_id { get; set; }

        [AliasAs("Client_secret")]
        public string Client_secret { get; set; }

    }
}
