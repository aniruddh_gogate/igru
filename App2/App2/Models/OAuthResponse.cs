﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Model
{
    public class OAuthResponse
    {
        [JsonProperty("access_token")]
        public string accessToken { get; set; }

        [JsonProperty("token_type")]
        public string tokenType { get; set; }

        [JsonProperty("expires_in")]
        public int expiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string refreshToken { get; set; }

        [JsonProperty("client_id")]
        public string clientId { get; set; }

        [JsonProperty("userName")]
        public string userName { get; set; }

        [JsonProperty(".persistent")]
        public string persistent { get; set; }

        [JsonProperty(".issued")]
        public string issued { get; set; }

        [JsonProperty(".expires")]
        public string expired { get; set; }
    }
}
