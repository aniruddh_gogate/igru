﻿using App2.Models;
using App2.Services;
using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace App2.Model
{
    public class Walklist
    {
        [JsonProperty("Walklist")]
        public List<Subject> Subjects { get; set; }


        public void CheckInSubject(Subject subject)
        {
            subject = Subjects.Find(updatedSubject => updatedSubject == subject);
            if (subject != null)
            {
                subject.Status = "101";
            }
        }
    }
}
