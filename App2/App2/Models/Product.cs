﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class Product
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("SKU")]
        public string SKU { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Cost")]
        public float? Cost { get; set; }

        [JsonProperty("Recurring")]
        public string Recurring { get; set; }
    }
}
