﻿using Newtonsoft.Json;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;

namespace iGRU.Models
{
    public class Knocks
    {
        public Knocks() { knocks = new List<KnockEventRequest>(); }

        [AliasAs("knocks")]
        public List<KnockEventRequest> knocks { get; set; }
    }
}
