﻿using App2.Model;
using Newtonsoft.Json;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;

namespace iGRU.Models
{
    public class KnockEventRequest  
    {
        public KnockEventRequest() { }

        public KnockEventRequest(Subject subject)
        {
            Hid = subject.Hid;
            Status = subject.Status;
            Latitude = 38.89793;
            Longitude = -77.03657;
            Type = "GPS";
            Uncertainty = 100;
            Time = "2012-12-25T14:45:23.34-05:00";
            Batterylevel = 99;
            Signalstrength = -120;
            Runningappscount = 30;
            Brightness = 200;
            Bluetooth = true;
            Bondeddevices = 2;
            Wifi = false;
            Imei = "357369040287979";
            Phonemodel = "LG Tab 4";
            Networktype = "EDGE";
        }

        [AliasAs("Hid")]
        public int Hid { get; set; }

        [AliasAs("Status")]
        public string Status { get; set; }

        [AliasAs("Latitude")]
        public double Latitude { get; set; }

        [AliasAs("Longitude")]
        public double Longitude { get; set; }

        [AliasAs("Type")]
        public string Type { get; set; }

        [AliasAs("Uncertainty")]
        public int Uncertainty { get; set; }

        [AliasAs("Time")]
        public string Time { get; set; }

        [AliasAs("Batterylevel")]
        public int Batterylevel { get; set; }

        [AliasAs("Signalstrength")]
        public int Signalstrength { get; set; }

        [AliasAs("Runningappscount")]
        public int Runningappscount { get; set; }

        [AliasAs("Brightness")]
        public float Brightness { get; set; }

        [AliasAs("Bluetooth")]
        public bool Bluetooth { get; set; }

        [AliasAs("Bondeddevices")]
        public int Bondeddevices { get; set; }

        [AliasAs("Wifi")]
        public bool Wifi { get; set; }

        [AliasAs("Imei")]
        public string Imei { get; set; }

        [AliasAs("Phonemodel")]
        public string Phonemodel { get; set; }

        [AliasAs("Networktype")]
        public string Networktype { get; set; }
    }
}
