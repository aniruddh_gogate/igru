﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class Question
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("TypeId")]
        public int TypeId { get; set; }

        [JsonProperty("Order")]
        public int Order { get; set; }

        [JsonProperty("Required")]
        public bool Required { get; set; }

        [JsonProperty("Magic")]
        public bool Magic { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Text")]
        public string Text { get; set; }

        [JsonProperty("Variable")]
        public string Variable { get; set; }

        [JsonProperty("QuestionTally")]
        public bool QuestionTally { get; set; }

        [JsonProperty("Restrictions")]
        public string Restrictions { get; set; }

        [JsonProperty("Hidden")]
        public bool Hidden { get; set; }

        [JsonProperty("EnableBarcode")]
        public bool EnableBarcode { get; set; }

        [JsonProperty("Branch")]
        public int Branch { get; set; }

        [JsonProperty("MinAnswers")]
        public int? MinAnswers { get; set; }

        [JsonProperty("MaxAnswers")]
        public int? MaxAnswers { get; set; }  

    }
}
