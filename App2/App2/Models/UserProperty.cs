﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class UserProperty
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("QuestionId")]
        public int QuestionId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Value")]
        public string Value { get; set; }

        [JsonProperty("PropertyType")]
        public int PropertyType { get; set; }
    }
}
