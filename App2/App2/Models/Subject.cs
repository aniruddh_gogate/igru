﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace App2.Model
{
    public class Subject
    {
        [JsonProperty("Hid")]
        public int Hid { get; set; }

        [JsonProperty("ExternalId1")]
        public string ExternalId1 { get; set; }

        [JsonProperty("ExternalId2")]
        public string ExternalId2 { get; set; }

        [JsonProperty("ExternalId3")]
        public string ExternalId3 { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Salutation")]
        public string Salutation { get; set; }

        [JsonProperty("Suffix")]
        public string Suffix { get; set; }

        [JsonProperty("Address")]
        public string Address { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("Zip")]
        public string Zip { get; set; }

        [JsonProperty("Apartment")]
        public string Apartment { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Cell")]
        public string Cell { get; set; }
        
        private string status;
        [JsonProperty("Status")]
        public string Status
        {
            get
            {
                if(status.Equals("0") || status == "Not Knocked")
                {
                    return "Not Knocked";
                }
                else if(status.Equals("101") || status.Equals("Checked-In"))
                {
                    return "Checked-In";
                }

                return "Not Knocked";
            }
            set
            {
                status = value;
            }
        }

        [JsonProperty("Other")]
        public SubjectOther Other { get; set; }

        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        [JsonProperty("Color")]
        public string Color { get; set; }

        [JsonProperty("Location")]
        public string Location { get; set; }

        [JsonProperty("Note")]
        public string Note { get; set; }


        public string FullName
        {
            get { return FirstName + " " + LastName; }
            set { ; }
        }

        public string CellColor
        {
            get
            {
                if(Status == "101" || Status == "Checked-In")
                {
                    return "LightGreen";
                }
                return "White";
            }
            set {; }
        }

    }
}
