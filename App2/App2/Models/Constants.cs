﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App2.Model
{
    public static class Constants
    {
        public static string AppName = "Grassroots Unwired";
        public static string BaseURLStaging = "https://staging.grassrootsunwired.com/api";
        

        public static class Endpoints
        {
            public static string loginOauth = "/token";
            public static string petitionGet = "/v2/petition/get";
            public static string walklistAll = "/v2/walklist/get?allLocations=1";
            public static string knockEvent = "/v2/events/knock";

        }

        public static class URLS
        {
            public static string loginURL = BaseURLStaging + Endpoints.loginOauth;
            public static string walklistAllURL = BaseURLStaging + Endpoints.walklistAll;
            public static string petitionGetURL = BaseURLStaging + Endpoints.petitionGet;
            public static string knockEventURL = BaseURLStaging + Endpoints.knockEvent;
        }

        public static class Colors
        {
            public static Color AppThemeColor = Color.LightGreen;
            public static Color Background_Color = Color.LightGray;
            public static Color MainTextColor = Color.DarkGray;
            public static Color ButtonColor = Color.LightGreen;
        }
    }
}
