﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Model
{
    public class EventInfo
    {
        [JsonProperty("Latitude")]
        private double Latitude { get; set; }
        [JsonProperty("Longitude")]
        private double Longitude { get; set; }

        [JsonProperty("Type")]
        private string Type { get; set; }

        [JsonProperty("Uncertainty")]
        private int Uncertainty { get; set; }

        [JsonProperty("Time")]
        private string Time { get; set; }

        [JsonProperty("Batterylevel")]
        private int Batterylevel { get; set; }

        [JsonProperty("Signalstrength")]
        private int Signalstrength { get; set; }

        [JsonProperty("Runningappscount")]
        private int Runningappscount { get; set; }

        [JsonProperty("Brightness")]
        private float Brightness { get; set; }

        [JsonProperty("Bluetooth")]
        private bool Bluetooth { get; set; }

        [JsonProperty("Bondeddevices")]
        private int Bondeddevices { get; set; }

        [JsonProperty("Wifi")]
        private bool Wifi { get; set; }

        [JsonProperty("Imei")]
        private string Imei { get; set; }

        [JsonProperty("Phonemodel")]
        private string Phonemodel { get; set; }

        [JsonProperty("Networktype")]
        private string Networktype { get; set; }


        public EventInfo()
        {
            this.Latitude = 38.89793;
            this.Longitude = -77.03657;
            this.Type = "GPS";
            this.Uncertainty = 100;
            this.Time = "2012-12-25T14:45:23.34-05:00";
            this.Batterylevel = 99;
            this.Signalstrength = -120;
            this.Runningappscount = 30;
            this.Brightness = 200;
            this.Bluetooth = true;
            this.Bondeddevices = 2;
            this.Wifi = false;
            this.Imei = "357369040287979";
            this.Phonemodel = "LG Tab 4";
            this.Networktype = "EDGE";
        }
}
}
