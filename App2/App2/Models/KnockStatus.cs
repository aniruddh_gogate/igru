﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class KnockStatus
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("LocalId")]
        public int LocalId { get; set; }

        [JsonProperty("CategoryType")]
        public int CategoryType { get; set; }

        [JsonProperty("CategoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}
