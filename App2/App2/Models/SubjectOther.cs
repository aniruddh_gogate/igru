﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Model
{
    public class SubjectOther
    {
        [JsonProperty("ParticipationType")]
        public string ParticipationType { get; set; }

        [JsonProperty("EmailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("DateofBirth")]
        public string DateofBirth { get; set; }

        [JsonProperty("Gender")]
        public string Gender { get; set; }

        [JsonProperty("AgeonRaceDay")]
        public string AgeonRaceDay { get; set; }

        [JsonProperty("Ethnicity")]
        public string Ethnicity { get; set; }

        [JsonProperty("TShirtSize")]
        public string TShirtSize { get; set; }

        [JsonProperty("Relationship")]
        public string Relationship { get; set; }

        [JsonProperty("Bib")]
        public string Bib { get; set; }

        [JsonProperty("GRUStatus")]
        public string GRUStatus { get; set; }
    }
}
