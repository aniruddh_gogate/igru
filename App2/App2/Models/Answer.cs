﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class Answer
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Question")]
        public int Question { get; set; }

        [JsonProperty("Value")]
        public string Value { get; set; }

        [JsonProperty("Branch")]
        public int? Branch { get; set; }
    }
}
