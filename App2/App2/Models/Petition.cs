﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class Petition
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Introduction")]
        public string Introduction { get; set; }

        [JsonProperty("Signature")]
        public bool Signature { get; set; }

        [JsonProperty("Walklist")]
        public bool Walklist { get; set; }

        [JsonProperty("CreditCheck")]
        public bool CreditCheck { get; set; }

        [JsonProperty("Petition")]
        public string PetitionValue { get; set; }

        [JsonProperty("Questions")]
        public List<Question> Questions { get; set; }

        [JsonProperty("Answers")]
        public List<Answer> Answers { get; set; }

        [JsonProperty("UserFirstName")]
        public string UserFirstName { get; set; }

        [JsonProperty("UserLastName")]
        public string UserLastName { get; set; }

        [JsonProperty("POS")]
        public bool POS { get; set; }

        [JsonProperty("Products")]
        public List<Product> Products { get; set; }

        [JsonProperty("Reservation")]
        public bool Reservation { get; set; }

        [JsonProperty("Bidirectional")]
        public bool Bidirectional { get; set; }

        [JsonProperty("LocationList")]
        public bool LocationList { get; set; }

        [JsonProperty("ScriptProperties")]
        public List<ScriptProperty> ScriptProperties { get; set; }

        [JsonProperty("QuestionProperties")]
        public List<QuestionProperty> QuestionProperties { get; set; }

        [JsonProperty("AnswerProperties")]
        public List<AnswerProperty> AnswerProperties { get; set; }

        [JsonProperty("ProductProperties")]
        public List<ProductProperty> ProductProperties { get; set; }

        [JsonProperty("UserProperties")]
        public List<UserProperty> UserProperties { get; set; }

        [JsonProperty("KnockStatuses")]
        public List<KnockStatus> KnockStatuses { get; set; }

        [JsonProperty("PaymentProcessorId")]
        public int PaymentProcessorId { get; set; }

        [JsonProperty("CampaignID")]
        public int CampaignID { get; set; }

        [JsonProperty("CampaignName")]
        public string CampaignName { get; set; }

        [JsonProperty("CrewNotes")]
        public string CrewNotes { get; set; }

        [JsonProperty("IsTraining")]
        public bool IsTraining { get; set; }

        [JsonProperty("LocationName")]
        public string LocationName { get; set; }

    }

}
