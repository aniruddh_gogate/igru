﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Models
{
    public class ScriptProperty
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("PetitionId")]
        public int PetitionId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Value")]
        public string Value { get; set; }

        [JsonProperty("PropertyType")]
        public int PropertyType { get; set; }

        [JsonProperty("Data")]
        public string Data { get; set; }

        [JsonProperty("ContentType")]
        public string ContentType { get; set; }
    }
}
