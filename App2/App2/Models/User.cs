﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Model
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }

        // Constructors
        public User() { }
        
        public User(string Username, string Password)
        {
            this.Username = Username;
            this.Password = Password;
        }

        public bool IsLoginValid()
        {
            if(this.Username == null || this.Password == null || this.Username.Equals("") || this.Password.Equals(""))
            {
                return false;
            }

            return true;
        }
    }
}
