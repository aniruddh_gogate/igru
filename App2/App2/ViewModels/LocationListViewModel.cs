﻿using App2.Data;
using App2.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2.ViewModels
{
    public class LocationListViewModel : BaseViewModel
    {

        public LocationListViewModel()
        {
            WalklistCommand = new Command(async () => await onWalklistClicked());
            this.WalklistCommand.Execute(onWalklistClicked());
            Title = "Alpha List";
        }

        public Command WalklistCommand { get; }


        private Walklist subjectList;
        public Walklist SubjectList
        {
            get { return subjectList; }
            set
            {
                SetProperty(ref subjectList, value);
            }
        }


        private ObservableRangeCollection<Subject> subjects;

        public ObservableRangeCollection<Subject> Subjects
        {
            get { return subjects; }
            set
            {
                SetProperty(ref subjects, value);
            }
        }



        private Subject selectedSubject;

        public Subject SelectedSubject
        {
            get
            {
                return selectedSubject;
            }

            set
            {
                selectedSubject = value;                            
            }
           
        }
    

        async Task onWalklistClicked()
        {
            IsBusy = true;
            HttpResponseMessage httpResponse = await GrassrootsRestClient.GetWalklistAll();

            if (httpResponse.IsSuccessStatusCode)
            {
                SubjectList = JsonConvert.DeserializeObject<Walklist>(await httpResponse.Content.ReadAsStringAsync());
                Subjects = new ObservableRangeCollection<Subject>(SubjectList.Subjects);
                // Do walklist stuff
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Login failed", "", "OK");
            }

            IsBusy = false;
        }

        public async void CheckIn(Subject subject)
        {
            IsBusy = true;

            HttpResponseMessage responseMessage = await GrassrootsRestClient.KnockEvent(subject);

            if (responseMessage.IsSuccessStatusCode)
            {            
                //await Application.Current.MainPage.DisplayAlert("Knock sent to server", "Yayy", "OK");
            }
            
            IsBusy = false;
        }


        public void UpdateAlphalist()
        {
            Subjects = new ObservableRangeCollection<Subject>(SubjectList.Subjects);
        }

    }
}
