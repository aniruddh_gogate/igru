﻿using App2.Data;
using App2.Model;
using App2.Models;
using App2.Views;
using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
       
        public LoginViewModel()
        {
            LoginCommand = new Command(async ()=> await onLoginClicked());
        }

        string username = "";
        string password = "";
        public INavigation Navigation;


        public string Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        
        public Command LoginCommand { get;  }


        async Task onLoginClicked()
        {
            IsBusy = true;

            HttpResponseMessage authResponse = await GrassrootsRestClient.Login(Username, Password);

            if (authResponse.IsSuccessStatusCode)
            {
                OAuthResponse oAuthResponse = JsonConvert.DeserializeObject<OAuthResponse>(await authResponse.Content.ReadAsStringAsync());
                GrassrootsRestClient.Authorization = oAuthResponse.accessToken;
                GrassrootsRestClient.Imei = "000000000000000";

                // Petition GET
                HttpResponseMessage petitionResponse = await GrassrootsRestClient.GetPetition();
                Petition petition = JsonConvert.DeserializeObject<Petition>(await petitionResponse.Content.ReadAsStringAsync());

                await Navigation.PushAsync(new LocationListPage());
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Login failed", "", "OK");
            }

            IsBusy = false;
        }       
    }
}

