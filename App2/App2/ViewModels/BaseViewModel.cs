﻿using App2.Data;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace App2.ViewModels
{
    public class BaseViewModel : ObservableObject
    {
        public IGrassrootsRestClient GrassrootsRestClient { get; }
        public BaseViewModel()
        {
            GrassrootsRestClient = DependencyService.Get<IGrassrootsRestClient>();
        }

        string title;
        public string Title
        {
            get => title;
            set
            {
                if (title == value)
                    return;
                title = value;
                OnPropertyChanged();
            }
        }

        bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                if (isBusy == value)
                    return;
                isBusy = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsNotBusy));
            }
        }

        public bool IsNotBusy => !IsBusy;

    }
}
