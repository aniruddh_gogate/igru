﻿using App2.Model;
using Refit;
using Newtonsoft;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App2.Data

{
    public interface IGrassrootsRestClient
    {
        string Authorization { get; set; }
        string Imei { get; set; }

        Task<HttpResponseMessage> Login(string Username, string Password);
        Task<HttpResponseMessage> GetWalklistAll();
        Task<HttpResponseMessage> GetPetition();
        Task<HttpResponseMessage> KnockEvent(Subject subject);

    }
}
