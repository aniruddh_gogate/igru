﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using App2.Model;
using System.Threading.Tasks;
using static Newtonsoft.Json.JsonConvert;
using System.Net.Http.Headers;
using App2.Data;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;
using Plugin.Settings.Abstractions;
using Plugin.Settings;
using App2.Services;
using iGRU.Models;

[assembly: Dependency(typeof(GrassrootsRestClient))]
namespace App2.Services
{
    public class GrassrootsRestClient : IGrassrootsRestClient
    {
        static HttpClient client = new HttpClient();

        private static ISettings AppSettings => CrossSettings.Current;

        public string Authorization
        {
            get => AppSettings.GetValueOrDefault(nameof(Authorization), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(Authorization), value);
        }

        public string Imei
        {
            get => AppSettings.GetValueOrDefault(nameof(Imei), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(Imei), value);
        }
     

        public GrassrootsRestClient()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 5956000;
            var accept = "application/json";
            client.DefaultRequestHeaders.Add("Accept", accept);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Authorization);
            client.DefaultRequestHeaders.Add("imei", Imei);
        }

        /**
         * Login API call
         **/ 

        public async Task<HttpResponseMessage> Login(string Username, string Password)
        {         
            OAuthRequest logintest = new OAuthRequest(Username, Password);
            return await PostAsyncEncoded(Constants.URLS.loginURL, logintest) ;   
        }

        /**
         * Get Walklist for all locations
         */

        public async Task<HttpResponseMessage> GetWalklistAll()
        {
            return await GetAsync(Constants.URLS.walklistAllURL);   
        }

        /**
         * Petition GET 
         */

        public async Task<HttpResponseMessage> GetPetition()
        {
            return await GetAsync(Constants.URLS.petitionGetURL);
        }

        /**
         * KnockEvent PUT
         */
        public async Task<HttpResponseMessage> KnockEvent(Subject subject)
        {
            KnockEventRequest knockEventRequest = new KnockEventRequest(subject);
            Knocks bulkKnocks = new Knocks();
            bulkKnocks.knocks.Add(knockEventRequest);
            return await PutAsyncEncoded(Constants.URLS.knockEventURL, bulkKnocks);
        }



        /**
         *API Helper Methods 
         */

        private static Task<HttpResponseMessage> PostAsyncEncoded(string uri, object content)
        {
            var json = JsonConvert.SerializeObject(content);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            return SendAsync(new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(dictionary) });
        }

        private static Task<HttpResponseMessage> GetAsyncEncoded(string uri, object content)
        {
            var json = JsonConvert.SerializeObject(content);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            return SendAsync(new HttpRequestMessage(HttpMethod.Get, uri) { Content = new FormUrlEncodedContent(dictionary) });
        }

        private static Task<HttpResponseMessage> PutAsyncEncoded(string uri, object content)
        {
            var json = JsonConvert.SerializeObject(content);            
            return SendAsync(new HttpRequestMessage(HttpMethod.Put, uri) { Content = new StringContent(json, Encoding.UTF8, "application/json") });
        }

        private static Task<HttpResponseMessage> GetAsync(string uri)
        {
            return SendAsync(new HttpRequestMessage(HttpMethod.Get, uri));
        }

        private static Task<HttpResponseMessage> PutAsync(string uri)
        {
            return SendAsync(new HttpRequestMessage(HttpMethod.Put, uri));
        }


        private static Task<HttpResponseMessage> PostAsync(string uri, HttpContent content)
        {
            return SendAsync(new HttpRequestMessage(HttpMethod.Post, uri) { Content = content });
        }

        private static Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            return SendAsync(request, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
        }

        private static Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, HttpCompletionOption option, CancellationToken cToken)
        {
            if (client != null && request != null)
            {
                return client.SendAsync(request, option, cToken);
            }
            else
            {
                return null;
            }
        }
    }
}
