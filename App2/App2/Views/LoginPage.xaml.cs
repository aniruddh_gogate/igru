﻿using App2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using App2.Data;
using Refit;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;
using App2.ViewModels;

namespace App2.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
               
        public LoginPage (LoginViewModel loginViewModel)
		{
			InitializeComponent ();
            loginViewModel.Navigation = Navigation;
            BindingContext = loginViewModel;
            
		}
    }
}