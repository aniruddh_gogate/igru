﻿using App2.Model;
using App2.Services;
using App2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LocationListPage : ContentPage
	{
        private LocationListViewModel locationListViewModel = new LocationListViewModel();
		public LocationListPage ()
		{
             InitializeComponent();
             BindingContext = locationListViewModel;

        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Subject selectedSubject = ((ListView)sender).SelectedItem as Subject;
            if(selectedSubject != null)
            {
            
                bool Ok = await DisplayAlert("Check-In", "Do you wanna check-in " + selectedSubject.FullName + "?", "OK", "Cancel");
                {
                    if(Ok)
                    {
                        selectedSubject.Status = "101";
                        locationListViewModel.SubjectList.CheckInSubject(selectedSubject);
                        locationListViewModel.CheckIn(selectedSubject);
                        locationListViewModel.UpdateAlphalist();
                    }
                }
                
            }
        }
    }
}